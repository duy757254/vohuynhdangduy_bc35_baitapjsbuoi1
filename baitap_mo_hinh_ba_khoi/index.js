// 1. Viết chương trình tính chiều dài cạnh huyền của một tam giác vuông khi biết hai cạnh góc vuông.
/**
 * Đầu Vào:
 * edge1:7
 * edge2:8
 *
 * Các Bước Tiến Hành:
 *  khai các biến số edge1,edge2
 * edge1=7
 * edge2=8
 * Áp dụng theo định lý pytago
 * dùng Math.sqrt để cân bậc 2 tổng bình phương 2 cạnh góc vuông
 * chúng ta sẻ console để in ra HTML kiểm tra kết quả
 *
 * Đầu Ra:
 * edge3: 10.63014581273465
 *
 */

var edge1 = 7;
var edge2 = 8;

var edge3 = Math.sqrt(edge1 * edge1 + edge2 * edge2);
console.log("edge3:", edge3);

// 2. Viết Chương trình tính nhập vào số nguyên dương n với 3 ký số, tính và xuất tổng 3 ký số n
/**
 * Đầu vào:
 * số nguyên dương (n) : 794
 *
 *
 *
 * Các Bước Tiến Hành:
 * tách các co số 7 9 4 bằng công thức chia lấy dư
 * Dùng Math.floor để lấy số nguyên gần nhất
 * sau khi lấy được 3 ký số ta sẻ cộng 3 ký số lại
 * ta sẻ console.log để in ra html kiểm tra giá trị
 * 
 * Đầu Ra:
 * kết quả tổng của 3 ký số 7+9+4=20
 *
 *
 *
 *
 *
 */

var n = 794;
var hangdonvi = n % 10;
console.log("hangdonvi:", hangdonvi);
var hangchuc = Math.floor((n % 100) / 10);
console.log("hangchuc:", hangchuc);
var hangtram = Math.floor(n / 100);
console.log("hangtram:", hangtram);
var tong3kyso = hangdonvi + hangchuc + hangtram;
console.log("tong3kyso", tong3kyso)
