// Bài 1: Tính tiền lương nhân viên.
/**
 *
 * Đầu vào:
 * Lương 1 ngày:100.000
 * cho người dùng nhập số ngày làm (Ví dụ): 26 ngày
 * Các Bước tiến hành:
 * khai báo các biến số
 *  Lương 1 ngày: 100.000
 *  Số ngày làm việc: 26 ngày
 *  Tính số lương được hưởng trong 26 ngày
 * Đầu ra:
 *  Số lương nhân viên là 2.600.000
 *
 */

var salary1day = 100000;
// console.log("salary1day:", salary1day);

var datastaff = 26;
// console.log("datastaff:", datastaff);

var salarystaff = salary1day * datastaff;
console.log("Lương Nhân Viên:", salarystaff);

// Bài 2: Tính giá trị trung bình
/**
 *
 * Đầu vào:
 * lấy giá trị của 5 người dùng (tự cho)
 * Người dùng 1: 8477
 * Người dùng 2: 4513
 * Người dùng 3: 7437
 * Người dùng 4: 6451
 * Người dùng 5: 7979
 *
 * Các bước tiến bành
 * Khai báo các số thực của tất cả người dùng
 * Tìm giá trị trung bình
 * tổng giá trị của 5 người dùng là : 34857
 * Lấy tất cả giá trị 5 người dùng nhập cộng lại chia 5
 *
 * Đầu ra
 * Giá trị trung bình: 6971.4
 */

var user1 = 8477;
var user2 = 4513;
var user3 = 7437;
var user4 = 6451;
var user5 = 7979;
var total = user1 + user2 + user3 + user4 + user5;
// console.log("total:", total);
var average = total / 5;
console.log("Trung Bình:", average);

// Bài 3 Quy đổi tiền
/**
 * Đầu vào:
 *  Lấy giá trị user nhập (tự cho)
 *  252 USD
 *  1 USD = 23500 VND
 *
 * Các bước tiến hành:
 *  Khai báo 1USD=23500
 *  Nhập số liệu user nhập = 252
 *  Console kết quả
 *
 * Đầu ra
 *  252USD=5922000VND
 *
 */

var oneusd = 23500;
var userimportusd = 252;
var usdvnd = oneusd * userimportusd;
console.log("Usd Sang VND:", usdvnd);

// Bài 4 Tính Diện Tích Chu Vi Hình Chữ Nhật
/**
 * Đầu Vào
 *  Chiều dài: 241 
 *  Chiều rộng: 198 
 *
 * Các bước tiến hành
 * Khai báo chiều dài và chiều rộng
 * Ta áp dụng công thức diện tích hình chữ nhật và chu vi hình chữ nhật
 * Ta console in kết quả ra html để kiểm tra kết quả 2 công thức diện tích và chu vi
 *
 * Đầu ra
 * Diện tích = 47718
 * Chu vi = 878
 *
 */

var lengthrectangle = 241;
var widthrectangle = 198;
var arearectangle = lengthrectangle * widthrectangle;
console.log("Diện Tích:", arearectangle);
var perimeterrectangle = (lengthrectangle + widthrectangle) * 2;
console.log("Chu Vi:", perimeterrectangle);

// Bài 5 Tính Tổng 2 Ký Số
/**
 * Đầu vào:
 * Ta cho 2 ký số = 49
 *
 *
 * Các bước tiến hành:
 * Khai báo số 49
 * Tiến hành tách số bằng cách chia lấy dư
 * Rồi cộng ký số đã tách ra và dùng Math.floor để làm tròn
 * Console in kết quả ra HTML để kiếm tra kết quả
 *
 * Đầu ra
 * kết quả tổng của 49 là 13
 *
 */

var n = 49;
var unit = n % 10;
// console.log("hangdonvi:", unit);
var ten = Math.floor(n / 10);
// console.log("hangchuc:", ten)
var Result = unit + ten;
console.log("Kết Quả:", Result);
